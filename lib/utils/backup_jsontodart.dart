import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'dart:math' as math;

// Preparamos un FutureBuilder para decodificar el JSON
Future<ApiCodeStats> getData(String usuario) async {
  // Asignamos la respuesta de la Api a una variable para luego decodificarlas
  final response = await http.get('https://codestats.net/api/users/$usuario');

  //Decodificamos y mapeamos la información
  Map decode = json.decode(response.body);

  // Retorna a la clase donde se descompone la información
  return ApiCodeStats.fromJson(decode);
}

// Herramienta para descomponer un JSON al lenguaje Dart: https://javiercbk.github.io/json_to_dart/
class ApiCodeStats {
  Languages languages;
  Machines machines;
  int newXp;
  int totalXp;
  String user;

  ApiCodeStats(
      {this.languages, this.machines, this.newXp, this.totalXp, this.user});

  ApiCodeStats.fromJson(Map<String, dynamic> json) {
    languages = json['languages'] != null
        ? new Languages.fromJson(json['languages'])
        : null;
    machines = json['machines'] != null
        ? new Machines.fromJson(json['machines'])
        : null;
    newXp = json['new_xp'];
    totalXp = json['total_xp'];
    user = json['user'];
  }
}

class Languages {
  ExperienceData c;
  ExperienceData cSS;
  ExperienceData dart;
  ExperienceData docker;
  ExperienceData hTML;
  ExperienceData jSON;
  ExperienceData java;
  ExperienceData javaScript;
  ExperienceData markdown;
  ExperienceData python;
  ExperienceData sCSS;
  ExperienceData typeScript;
  ExperienceData xML;
  ExperienceData arduinoOutput;

  Languages(
      {this.c,
      this.cSS,
      this.dart,
      this.docker,
      this.hTML,
      this.jSON,
      this.java,
      this.javaScript,
      this.markdown,
      this.python,
      this.sCSS,
      this.typeScript,
      this.xML,
      this.arduinoOutput});

  Languages.fromJson(Map<String, dynamic> json) {
    c = json['C++'] != null ? new ExperienceData.fromJson(json['C++']) : null;
    cSS = json['CSS'] != null ? new ExperienceData.fromJson(json['CSS']) : null;
    dart =
        json['Dart'] != null ? new ExperienceData.fromJson(json['Dart']) : null;
    docker = json['Docker'] != null
        ? new ExperienceData.fromJson(json['Docker'])
        : null;
    hTML =
        json['HTML'] != null ? new ExperienceData.fromJson(json['HTML']) : null;
    jSON =
        json['JSON'] != null ? new ExperienceData.fromJson(json['JSON']) : null;
    java =
        json['Java'] != null ? new ExperienceData.fromJson(json['Java']) : null;
    javaScript = json['JavaScript'] != null
        ? new ExperienceData.fromJson(json['JavaScript'])
        : null;
    markdown = json['Markdown'] != null
        ? new ExperienceData.fromJson(json['Markdown'])
        : null;
    python = json['Python'] != null
        ? new ExperienceData.fromJson(json['Python'])
        : null;
    sCSS =
        json['SCSS'] != null ? new ExperienceData.fromJson(json['SCSS']) : null;
    typeScript = json['TypeScript'] != null
        ? new ExperienceData.fromJson(json['TypeScript'])
        : null;
    xML = json['XML'] != null ? new ExperienceData.fromJson(json['XML']) : null;
    arduinoOutput = json['arduino-output'] != null
        ? new ExperienceData.fromJson(json['arduino-output'])
        : null;
  }
}

class ExperienceData {
  int newXps;
  int xps;

  ExperienceData({this.newXps, this.xps});

  ExperienceData.fromJson(Map<String, dynamic> json) {
    newXps = json['new_xps'];
    xps = json['xps'];
  }
}

class Machines {
  ExperienceData cYS;
  ExperienceData hacklife;

  Machines({this.cYS, this.hacklife});

  Machines.fromJson(Map<String, dynamic> json) {
    cYS = json['CYS'] != null ? new ExperienceData.fromJson(json['CYS']) : null;
    hacklife = json['Hacklife'] != null
        ? new ExperienceData.fromJson(json['Hacklife'])
        : null;
  }
}

int getLevel(int exptotal) => (0.025 * math.sqrt(exptotal)).floor();

double getPercentLevel(int experiencia) =>
    (0.025 * math.sqrt(experiencia) - getLevel(experiencia)) / 1;

List<double> fechas = [
  178.0,
  11929.0,
  6072.0,
  5680.0,
  3786.0,
  3554.0,
  4430.0,
  123.0,
  972.0,
  2518.0,
  3054.0,
  2282.0,
  5623.0,
  1321.0,
  1347.0,
  7600.0,
  3710.0,
  6181.0,
  1269.0,
  3053.0,
  4272.0,
  1234.0,
  859.0,
  850.0,
  487.0,
  6717.0,
  2117.0,
  85.0,
  2021.0,
  1281.0,
  3498.0,
  326.0,
  3.0,
  1416.0,
  631.0,
  3535.0,
  1792.0,
  3127.0,
  21.0,
  5709.0,
  250.0,
  1225.0,
  3326.0,
  583.0,
  17.0,
  719.0,
  2663.0,
  2704.0,
  1911.0,
  700.0,
  1426.0,
  787.0,
  1635.0,
  1666.0,
  593.0,
  3549.0,
  6546.0,
  2368.0,
  1720.0,
  277.0,
  3.0,
  11639.0,
  479.0,
  7331.0,
  3861.0,
  2494.0,
  3509.0,
  654.0,
  4237.0,
  3356.0,
  2821.0,
  2670.0,
  4253.0,
  878.0,
  3752.0,
  1333.0,
  533.0,
  2834.0,
  3478.0,
  3884.0,
  2385.0,
  965.0,
  6386.0,
  476.0,
  3037.0,
  787.0,
  2069.0,
  310.0,
  2922.0,
  3886.0,
  4147.0,
  2980.0,
  3629.0,
  10726.0,
  3855.0,
  1.0,
  35.0,
  122.0,
  332.0,
  1809.0,
  5026.0,
  2186.0,
  1107.0,
  2427.0,
  2319.0,
  2903.0,
  758.0,
  500.0,
  178.0,
  380.0,
  1447.0,
  3155.0,
  649.0,
  2357.0,
  922.0,
  770.0,
  199.0,
  353.0,
  581.0,
  2920.0,
  2755.0,
  987.0,
  4084.0,
  3429.0,
  659.0,
  6502.0
];
