import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'dart:math' as math;

// Preparamos un FutureBuilder para decodificar el JSON
Future<ApiCodeStats> getData(String usuario) async {
  // Asignamos la respuesta de la Api a una variable para luego decodificarlas
  final response = await http.get('https://codestats.net/api/users/$usuario');

  //Decodificamos y mapeamos la información
  Map decode = json.decode(response.body);

  // Retorna a la clase donde se descompone la información
  return ApiCodeStats.fromJson(decode);
}

// Herramienta para descomponer un JSON al lenguaje Dart: https://javiercbk.github.io/json_to_dart/
ApiCodeStats apiCodeStatsFromJson(String str) =>
    ApiCodeStats.fromJson(json.decode(str));

String apiCodeStatsToJson(ApiCodeStats data) => json.encode(data.toJson());

class ApiCodeStats {
  Map<String, int> dates;
  Map<String, Language> languages;
  Machines machines;
  int newXp;
  int totalXp;
  String user;

  ApiCodeStats({
    this.dates,
    this.languages,
    this.machines,
    this.newXp,
    this.totalXp,
    this.user,
  });

  factory ApiCodeStats.fromJson(Map<String, dynamic> json) => ApiCodeStats(
        dates:
            Map.from(json["dates"]).map((k, v) => MapEntry<String, int>(k, v)),
        languages: Map.from(json["languages"])
            .map((k, v) => MapEntry<String, Language>(k, Language.fromJson(v))),
        machines: Machines.fromJson(json["machines"]),
        newXp: json["new_xp"],
        totalXp: json["total_xp"],
        user: json["user"],
      );

  Map<String, dynamic> toJson() => {
        "dates": Map.from(dates).map((k, v) => MapEntry<String, dynamic>(k, v)),
        "languages": Map.from(languages)
            .map((k, v) => MapEntry<String, dynamic>(k, v.toJson())),
        "machines": machines.toJson(),
        "new_xp": newXp,
        "total_xp": totalXp,
        "user": user,
      };
}

class Language {
  int newXps;
  int xps;

  Language({
    this.newXps,
    this.xps,
  });

  factory Language.fromJson(Map<String, dynamic> json) => Language(
        newXps: json["new_xps"],
        xps: json["xps"],
      );

  Map<String, dynamic> toJson() => {
        "new_xps": newXps,
        "xps": xps,
      };
}

class Machines {
  Language cys;
  Language hacklife;

  Machines({
    this.cys,
    this.hacklife,
  });

  factory Machines.fromJson(Map<String, dynamic> json) => Machines(
        cys: Language.fromJson(json["CYS"]),
        hacklife: Language.fromJson(json["Hacklife"]),
      );

  Map<String, dynamic> toJson() => {
        "CYS": cys.toJson(),
        "Hacklife": hacklife.toJson(),
      };
}

int getLevel(int exptotal) => (0.025 * math.sqrt(exptotal)).floor();

double getPercentLevel(int experiencia) =>
    (0.025 * math.sqrt(experiencia) - getLevel(experiencia)) / 1;
