import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'dart:math' as math;

// Preparamos un FutureBuilder para decodificar el JSON
Future<ApiCodeStats> getData(String usuario) async {
  // Asignamos la respuesta de la Api a una variable para luego decodificarlas
  final response = await http.get('https://codestats.net/api/users/$usuario');

  //Decodificamos y mapeamos la información
  Map decode = json.decode(response.body);

  // Retorna a la clase donde se descompone la información
  return ApiCodeStats.fromJson(decode);
}

// Herramienta para descomponer un JSON al lenguaje Dart: https://javiercbk.github.io/json_to_dart/
ApiCodeStats apiCodeStatsFromJson(String str) => ApiCodeStats.fromJson(json.decode(str));

class ApiCodeStats {
  Map<String, int> dates;
  Map<String, Language> languages;
  Map<String, Machines> machines;
  int newXp;
  int totalXp;
  String user;

  ApiCodeStats({
    this.dates,
    this.languages,
    this.machines,
    this.newXp,
    this.totalXp,
    this.user,
  });

  factory ApiCodeStats.fromJson(Map<String, dynamic> json) => ApiCodeStats(
        dates: Map.from(json["dates"]).map((k, v) => MapEntry<String, int>(k, v)),
        languages: Map.from(json["languages"]).map((k, v) => MapEntry<String, Language>(k, Language.fromJson(v))),
        machines: Map.from(json["machines"]).map((k, v) => MapEntry<String, Machines>(k, Machines.fromJson(v))),
        newXp: json["new_xp"],
        totalXp: json["total_xp"],
        user: json["user"],
      );
}

class Language {
  int newXps;
  int xps;

  Language({
    this.newXps,
    this.xps,
  });

  factory Language.fromJson(Map<String, dynamic> json) => Language(
        newXps: json["new_xps"],
        xps: json["xps"],
      );
}

// LA CLASE DEBE SER IGUAL A LA DE LENGUAJES (Cambiar Lenguaje por Maquina) PARA OBTENER LOS DATOS EN FORMA DE LISTA
class Machines {
  int newXps;
  int xps;

  Machines({
    this.newXps,
    this.xps,
  });

  factory Machines.fromJson(Map<String, dynamic> json) => Machines(
        newXps: json["new_xps"],
        xps: json["xps"],
      );
}

//TODO: [API] Ordenar la lista de fechas PREVIAMENTE al llamado. Esto es en Api_Service.dart
/* List<String> ordenarAMayorValor(List<dynamic> date, List<dynamic> exp) {
    List fecha = date;
    List xp = exp;
    dynamic aux1;
    dynamic aux2;

    for (int k = 0; k < fecha.length; k++) {
      for (int v = 0; v < (fecha.length - k) - 1; v++) {
        if (xp[v] < xp[v + 1]) {
          aux1 = xp[v];
          xp[v] = xp[v + 1];
          xp[v + 1] = aux1;
          aux2 = fecha[v];
          fecha[v] = fecha[v + 1];
          fecha[v + 1] = aux2;
        }
      }
    }
    print("Pasa por la nueva funcion FECHAS");
    return fecha;
  } */

int getLevel(int exptotal) => (0.025 * math.sqrt(exptotal)).floor();

double getPercentLevel(int experiencia) => (0.025 * math.sqrt(experiencia) - getLevel(experiencia)) / 1;
