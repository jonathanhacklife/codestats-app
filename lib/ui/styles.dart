import 'package:flutter/material.dart';

String fuente = "DidactGothic";

//TITULOS
TextStyle h1 = TextStyle(
      fontFamily: fuente,
      fontSize: 52,
      letterSpacing: 0.2,
    );

TextStyle h2 = TextStyle(
      fontFamily: fuente,
      fontSize: 44,
    );

TextStyle h3 = TextStyle(
      fontFamily: fuente,
      fontSize: 32,
      fontWeight: FontWeight.bold,
      letterSpacing: 0.1,
    );

TextStyle h4 = TextStyle(
      fontFamily: fuente,
      fontSize: 26,
      letterSpacing: 0.2,
    );

TextStyle h5 = TextStyle(
      fontFamily: fuente,
      fontSize: 20,
      fontWeight: FontWeight.bold,
      letterSpacing: 0.2,
    );

TextStyle h6 = TextStyle(
      fontFamily: fuente,
      fontSize: 18,
      fontWeight: FontWeight.bold,
      letterSpacing: 0.2,
    );

TextStyle subtitle1 = TextStyle(
      fontFamily: "DidactGothic",
      fontSize: 16,
      letterSpacing: 0.1,
    );

TextStyle subtitle2 = TextStyle(
      fontFamily: "DidactGothic",
      fontSize: 14,
      letterSpacing: 0.1,
    );

NetworkImage imagenPerfil() {
  return NetworkImage(
      "https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/8e294e74-d137-4915-85cd-ded25907aa66/d9w6e5k-7b5300fe-f8ab-4c7f-aba3-e107cb422819.jpg/v1/fill/w_1024,h_681,q_75,strp/slenderwhat___by_jonathanhacklife_d9w6e5k-fullview.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7ImhlaWdodCI6Ijw9NjgxIiwicGF0aCI6IlwvZlwvOGUyOTRlNzQtZDEzNy00OTE1LTg1Y2QtZGVkMjU5MDdhYTY2XC9kOXc2ZTVrLTdiNTMwMGZlLWY4YWItNGM3Zi1hYmEzLWUxMDdjYjQyMjgxOS5qcGciLCJ3aWR0aCI6Ijw9MTAyNCJ9XV0sImF1ZCI6WyJ1cm46c2VydmljZTppbWFnZS5vcGVyYXRpb25zIl19.gJCkG_KKYeDJANMsg9DthBfzaT-3d0d0rEHgnbx19LU");
}

NetworkImage imagenBanner() {
  return NetworkImage(
      "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcReGw9S-0LhOVfJjDmdEwi6FTJ6np4NJMg5hg&usqp=CAU");
}
