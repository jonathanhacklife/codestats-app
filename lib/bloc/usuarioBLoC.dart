import 'dart:async';

class ConfigBLoC {
  String usuario = "jwildemer";

  StreamController _configController = StreamController();
  Stream get getStreamConfig => _configController.stream;

  String getUsuario() {
    _configController.sink.add(usuario);
    return usuario;
  }

  setUsuario(user) {
    usuario = user;
    _configController.sink.add(usuario);
    return _configController;
  }

  // Ordena la lista de lenguajes por experiencia total
  List<dynamic> ordenarAMayorValor(List<dynamic> leng, List<dynamic> exp) {
    List lenguaje = leng.toList();
    List xp = [];
    for (int x = 0; x < leng.toList().length; x++) xp.add(exp.toList()[x].xps);
    
    dynamic aux1;
    dynamic aux2;

    for (int k = 0; k < lenguaje.length; k++) {
      for (int v = 0; v < (lenguaje.length - k) - 1; v++) {
        if (xp[v] < xp[v + 1]) {
          aux1 = xp[v];
          xp[v] = xp[v + 1];
          xp[v + 1] = aux1;
          aux2 = lenguaje[v];
          lenguaje[v] = lenguaje[v + 1];
          lenguaje[v + 1] = aux2;
        }
      }
    }
    // ? Debería devolver un diccionario con sus valores correspondientes
    return lenguaje;
  }

  dispose() {
    _configController.close();
  }
}
