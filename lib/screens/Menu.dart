import 'package:codestatsapp/bloc/usuarioBLoC.dart';
import 'package:flutter/material.dart';
import 'package:codestatsapp/utils/api_service.dart';
import 'package:codestatsapp/ui/styles.dart';
import 'package:flutter_sparkline/flutter_sparkline.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:intl/intl.dart';

//* Seteamos nuestro BLoC
ConfigBLoC configBLoC = ConfigBLoC();

class CodestatsApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(title: 'CodeStats', theme: ThemeData.dark(), home: HomePage());
  }
}

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Mi perfil", style: h5), iconTheme: IconThemeData(color: Colors.white), backgroundColor: Colors.transparent, elevation: 0.0),
      drawer: Drawer(child: DrawerUsuario()),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 5),
        child: ListView(
          children: <Widget>[
            Column(
              children: <Widget>[
                // Hola, usuario
                Padding(
                  padding: EdgeInsets.all(10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[Text("Hola, ${configBLoC.getUsuario()}", style: h3, textAlign: TextAlign.left), CircleAvatar(backgroundImage: imagenPerfil(), radius: 25)],
                  ),
                ),

                // Experiencia por lenguaje
                Card(
                  // Titulo "Experiencia por lenguaje"
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: Row(
                          children: [Text("Experiencia por lenguaje", style: h5, textAlign: TextAlign.left)],
                        ),
                      ),

                      // Tarjetas
                      FutureBuilder<ApiCodeStats>(
                        future: getData(configBLoC.getUsuario()),
                        builder: (context, snapshot) {
                          if (snapshot.hasData) {
                            return Container(
                              // TODO: Buscar una forma más apropiada para ajustar el container
                              height: MediaQuery.of(context).size.height * 0.40,
                              child: ListView.builder(
                                shrinkWrap: true,
                                scrollDirection: Axis.horizontal,
                                itemBuilder: (BuildContext context, int index) {
                                  // Se repite 4 veces este fragmento debido a que se muestran los primeros 4 lenguajes en pantalla
                                  print(snapshot.data.languages.keys.toList().length);
                                  return CardLanguage(
                                      lenguaje: configBLoC.ordenarAMayorValor(snapshot.data.languages.keys.toList(), snapshot.data.languages.values.toList())[index],
                                      experienciaTotal: snapshot.data.languages[configBLoC.ordenarAMayorValor(snapshot.data.languages.keys.toList(), snapshot.data.languages.values.toList())[index]].xps,
                                      nuevaExperiencia: snapshot.data.languages[configBLoC.ordenarAMayorValor(snapshot.data.languages.keys.toList(), snapshot.data.languages.values.toList())[index]].newXps);
                                },
                                itemCount: snapshot.data.languages.keys.toList().length,
                              ),
                            );
                          } else {
                            return CircularProgressIndicator();
                          }
                        },
                      ),
                    ],
                  ),
                ),

                // Experiencia por fechas
                Card(
                  child: Column(
                    children: <Widget>[
                      // Título "Experiencia por fechas"
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: Row(
                          children: <Widget>[Text("Experiencia por fechas", style: h6, textAlign: TextAlign.left)],
                        ),
                      ),

                      // Gráfico de fechas
                      FutureBuilder<ApiCodeStats>(
                        future: getData(configBLoC.getUsuario()),
                        builder: (context, snapshot) {
                          if (snapshot.hasData) {
                            List<double> xpToDataGraphic = [];
                            for (var fecha in snapshot.data.dates.values.toList()) xpToDataGraphic.add(fecha.toDouble());
                            return Container(
                              child: Sparkline(
                                data: xpToDataGraphic,
                                fillMode: FillMode.below,
                                fillGradient: LinearGradient(
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                  colors: [
                                    Colors.blue[400],
                                    Colors.blueAccent[400],
                                  ],
                                ),
                              ),
                            );
                          } else {
                            return CircularProgressIndicator();
                          }
                        },
                      ),
                    ],
                  ),
                ),

                // Experiencia por máquinas
                Card(
                  child: Column(
                    children: <Widget>[
                      // Título "Experiencia por máquinas"
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: Row(
                          children: <Widget>[Text("Experiencia por Máquinas", style: h6)],
                        ),
                      ),

                      // Tarjetas de máquinas
                      Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            FutureBuilder<ApiCodeStats>(
                              future: getData(configBLoC.getUsuario()),
                              builder: (context, snapshot) {
                                if (snapshot.hasData) {
                                  return GridView.count(
                                    crossAxisCount: 2,
                                    shrinkWrap: true,
                                    primary: false,
                                    children: List.generate(
                                      snapshot.data.machines.keys.toList().length,
                                      (index) {
                                        return CircularExperience(
                                            encabezado: Text("${snapshot.data.machines.keys.toList()[index]}\nNivel: ${getLevel(snapshot.data.machines[snapshot.data.machines.keys.toList()[index]].xps)}",
                                                style: h5, textAlign: TextAlign.center),
                                            dato: snapshot.data.machines[snapshot.data.machines.keys.toList()[index]].xps);
                                      },
                                    ),
                                  );
                                } else {
                                  return CircularProgressIndicator();
                                }
                              },
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {});
        },
        tooltip: 'Actualizar',
        child: Icon(Icons.refresh),
      ),
    );
  }
}

class DrawerUsuario extends StatefulWidget {
  const DrawerUsuario({Key key}) : super(key: key);

  @override
  _DrawerUsuarioState createState() => _DrawerUsuarioState();
}

class _DrawerUsuarioState extends State<DrawerUsuario> {
  TextEditingController _usuarioInputController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        // Banner de información
        FutureBuilder<ApiCodeStats>(
          future: getData(configBLoC.getUsuario()),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return UserAccountsDrawerHeader(
                  accountName: Text("${configBLoC.getUsuario()} ⭐ [${getLevel(snapshot.data.totalXp)}]", style: h3),
                  accountEmail: Text(
                      "Usuario desde: ${DateFormat('dd/MM/yyyy').format(DateTime.parse(snapshot.data.dates.keys.toList()[0]))}\n"
                      "Ultima vez: ${DateFormat('dd/MM/yyyy').format(DateTime.parse(snapshot.data.dates.keys.toList()[(snapshot.data.dates.length) - 1]))}",
                      style: h6),
                  decoration: BoxDecoration(image: DecorationImage(image: imagenBanner(), fit: BoxFit.cover)));
            } else {
              return Center(
                  child: Padding(
                padding: EdgeInsets.all(50),
                child: CircularProgressIndicator(),
              ));
            }
          },
        ),

        // Lista de ítems
        ListTile(
          title: TextField(
            controller: _usuarioInputController,
            autofocus: true,
            decoration: InputDecoration(border: InputBorder.none, hintText: 'Ingrese un username'),
          ),
          trailing: IconButton(
              icon: Icon(Icons.done),
              onPressed: () {
                // TODO: Actualizar toda la aplicacion al setear el usuario (VER USUARIOBLOC.DART) | Investigar sobre StreamsBuilders y FutureBuilders
                configBLoC.setUsuario(_usuarioInputController.text);
              }),
        ),
        ListTile(title: Text("Inicio"), onTap: () {}),
        ListTile(title: Text("Experiencia Semanal"), onTap: () {}),
        ListTile(title: Text("Experiencia Mensual"), onTap: () {}),
      ],
    );
  }

  void dispose() {
    _usuarioInputController.dispose();
    super.dispose();
  }
}

class CircularExperience extends StatelessWidget {
  const CircularExperience({Key key, this.dato, this.encabezado}) : super(key: key);

  final encabezado;
  final dato;

  @override
  Widget build(BuildContext context) {
    return CircularPercentIndicator(
        animation: true,
        animationDuration: 3000,
        circularStrokeCap: CircularStrokeCap.round,
        radius: 130.0,
        lineWidth: 15.0,
        progressColor: Colors.blue[400],
        curve: Curves.fastOutSlowIn,

        // Asignamos porcentaje del dato (Tiene que ser de tipo Double)
        percent: getPercentLevel(dato),
        // TODO: Asignar tamaño automatico según el ancho del padre
        header: encabezado,
        center: Text("${(getPercentLevel(dato) * 100).round()}%", style: h1));
  }
}

class CardLanguage extends StatelessWidget {
  const CardLanguage({Key key, @required this.lenguaje, @required this.experienciaTotal, @required this.nuevaExperiencia, this.color}) : super(key: key);

  final String lenguaje;
  final int experienciaTotal;
  final int nuevaExperiencia;
  final Color color;

  @override
  Widget build(BuildContext context) {
    return Container(
      //Construcción de las tarjetas de lenguajes
      margin: EdgeInsets.symmetric(horizontal: 15),
      child: Column(
        children: <Widget>[
          // Título del Lenguaje
          Text(lenguaje, style: h2),
          Column(
            children: <Widget>[
              // Nivel de experiencia
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(Icons.stars),
                  Text(" ${getLevel(experienciaTotal)}", style: h3), // Obtener el nivel de experiencia total
                ],
              ),
              CircularExperience(dato: experienciaTotal),
              Text("Hoy: $nuevaExperiencia\nTotal: $experienciaTotal", style: h4, textAlign: TextAlign.center),
            ],
          ),
        ],
      ),
    );
  }
}

// ! Códigos de color para los diferentes lenguajes
/*children: <Widget>[
  CardLanguage(lenguaje: "Python", color: Colors.red[400]),
  CardLanguage(lenguaje: "Dart", color: Colors.pink[400]),
  CardLanguage(lenguaje: "Java", color: Colors.purple[400]),
  CardLanguage(lenguaje: "TypeScript", color: Colors.deepPurple[400]),
  CardLanguage(lenguaje: "XML", color: Colors.indigo[400]),
  CardLanguage(lenguaje: "HTML", color: Colors.blue[400]),
  CardLanguage(lenguaje: "C++", color: Colors.blueAccent[400]),
  CardLanguage(lenguaje: "JSON", color: Colors.cyan[400]),
  CardLanguage(lenguaje: "JavaScript", color: Colors.teal[400]),
  CardLanguage(lenguaje: "CSS", color: Colors.green[400]),
  CardLanguage(lenguaje: "SCSS", color: Colors.greenAccent[400]),
  CardLanguage(lenguaje: "Docker", color: Colors.lime[400]),
],*/
