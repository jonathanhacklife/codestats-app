# Codestats App

Una versión mobile de Code::Stats

Code::Stats es un servicio gratuito de seguimiento de estadísticas para programadores.
Se basa en otorgar puntos de experiencia por la cantidad de programación que hagas. Te permite observar cómo crecen tus niveles para cada lenguaje que utilices, identificar tus puntos fuertes y utilizar los datos para ver dónde tienes todavía margen de mejora.

Además te permite mostrar tu página de estadísticas personales a tus amigos y comparar tu progreso con el de los demás. Incluso puedes hacer una competición. O, si lo deseas, puedes mantener toda tu información en privado y disfrutarla en secreto.

![Screenshot](docs/Screenshot.png)

![](docs/ScreenshotV1.jpg)
